<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"/>
    <title>Book List</title>
</head>
<body>
    <div class="container">
    <header class="d-flex justify-content-between my-4 text-light rounded" style=background:rgb(24,56,75) >
            <h1 style= padding:20px><i class="fas fa-swatchbook"></i>Book List</h1>
            <div>
                <br>
                <a href="Create.php" class="btn btn-primary text-dark mx-3" style=padding:10px;background:rgb(60,179,113);font-size:20px;><b>Add New Book</b></a>
            </div>
        </header>
        <?php
        session_start();
        if (isset($_SESSION["create"])){
            ?>
            <div class="alert alert-success">
            <?php
            echo $_SESSION["create"];
            ?>
        </div>
        <?php
        unset($_SESSION["create"]);
        }
        ?>
        <?php
        if (isset($_SESSION["edit"])){
            ?>
            <div class="alert alert-success">
            <?php
            echo $_SESSION["edit"];
            ?>
        </div>
        <?php
        unset($_SESSION["edit"]);
        }
        ?>
        <?php
        if (isset($_SESSION["delete"])){
            ?>
            <div class="alert alert-success">
            <?php
            echo $_SESSION["delete"];
            ?>
        </div>
        <?php
        unset($_SESSION["delete"]);
        }
        ?>
        <table class="table table-striped table-dark table-bordered">
            <thead>
                <tr style=font-size:25px;>
                    <th style=padding:15px;>#</th>
                    <th style=padding:15px;>Title</th>
                    <th style=padding:15px;>Author</th>
                    <th style=padding:15px;>Type</th>
                    <th style=padding:15px;>Action</th>                    
                </tr>
            </thead>
            <tbody style=font-size:20px;>
                <?php
                include ('connect.php');
                $sql = "SELECT * FROM Books";
                $result = mysqli_query($conn,$sql);
            
                while($row = mysqli_fetch_array($result)){
                ?>
                <tr>
                    <td><?php echo $row["id"];?></td>
                    <td><?php echo $row["title"];?></td>
                    <td><?php echo $row["author"];?></td>
                    <td><?php echo $row["type"];?></td>
                    <td>
                        <a href="view.php?id=<?php echo $row["id"];?>" class="btn btn-info text-dark" style=font-size:18px;><b>Read More</b></a>
                        <a href="edit.php?id=<?php echo $row["id"];?>" class="btn btn-warning" style=font-size:18px;><i class="fas fa-edit"></i> <b>Edit</b></a>
                        <a href="delete.php?id=<?php echo $row["id"];?>" class="btn btn-danger" style=font-size:18px;><i class="fas fa-trash-alt"></i> <b>Delete</b></a>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>

        </table>
    </div>
</body>
</html>